package com.example.hpelitebook.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Request.requestHttpsEjemplo();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
